package com.example.demo;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.util.Units;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFPicture;
import org.apache.poi.xwpf.usermodel.XWPFRun;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping(path="/defects")
public class MainController {
	
	@Autowired 
	private ObservationRepository observationRepository;

	@Autowired 
	private UserRepository userRepository;
	
	@Autowired 
	private AttachmentRepository attachmentRepository;
	
	
	@PostMapping (path="/addObservation")
	public String addNewObservation (@Autowired Observation observation) {
		if(null == observation.getObservationId()) {
			observation.setCreatedOn(new Date());
		}
		if(null == observation.getCreatedOn()) {
			observation.setCreatedOn(new Date());
		}
		observationRepository.save(observation);
		return "redirect:/defects/observation/"+observation.getObservationId();
	}

	@GetMapping(path="/search")
	public String getAllObservations(Model model) {
		model.addAttribute("users", userRepository.findAll());
		return "observations";
	}
	
	@PostMapping(path="/searchbyquery")
	public String searchObservationsByQuery(Model model, @RequestParam String createdBy, @RequestParam String reported) {
		Iterable<Observation> observations = null;
		if(createdBy.equalsIgnoreCase("all")) {
			observations = observationRepository.filterByReported(reported);
		} else {
			observations = observationRepository.filterByCreatedByReported(createdBy, reported);
		}
		model.addAttribute("observations", observations);
		model.addAttribute("users", userRepository.findAll());
		return "observations";
	}
	
	@GetMapping(path="/observation/{observationId}")
	public String getObservationById(@PathVariable Integer observationId, Model model) {
		model.addAttribute("users", userRepository.findAll());
		Observation observation = observationRepository.findOne(observationId);
		model.addAttribute("observation", observation);
		return "createobservation";
	}
	
	@GetMapping(path="/observation/new")
	public String createNewObservation(Model model) {
		model.addAttribute("users", userRepository.findAll());
		Observation observation = new Observation();
		observation.setReported("N");
		model.addAttribute("observation", observation);
		return "createobservation";
	}
	
	@PostMapping(path="/login")
	public String login(@RequestParam String userId, @RequestParam String password) {
		User user = userRepository.findOne(userId);
		if(null != user && user.getPassword().equals(password)) {
			return "dashboard";
		}
		return "error";
	}
	
	@GetMapping(path="/attachments/{observationId}")
	public String getAttachments(@PathVariable Integer observationId, Model model) {
		Iterable<Attachment> attachments = attachmentRepository.getAttachmentsByObervationId(observationId);
		model.addAttribute("files", attachments);
		model.addAttribute("observation", observationRepository.findOne(observationId));
		Attachment attachment = new Attachment();
		attachment.setObservationId(observationId);
		model.addAttribute("attachment", attachment);
		return "uploadfiles";
	}
	
	@PostMapping (path="/addattachment")
	public String addAttachment(@Autowired Attachment attachment) {
		attachmentRepository.save(attachment);
		return "redirect:/defects/attachments/"+attachment.getObservationId();
	}
	
	@GetMapping (path="/deleteattachment/{observationId}/{attachmentId}")
	public String deleteAttachment(@PathVariable Integer attachmentId, @PathVariable Integer observationId) {
		attachmentRepository.delete(attachmentId);
		return "redirect:/defects/observation/"+observationId;
	}
	
	@GetMapping (path="/filterByObservationIds/{observationIds}")
	public String filterByObservationIds(@PathVariable String observationIds, Model model) {
		if(null != observationIds) {
			List<Integer> observationIdList = new ArrayList<Integer>();
			String observationIdsArray[] = observationIds.split(",");
			for(String observationId:observationIdsArray) {
				observationIdList.add(Integer.parseInt(observationId));
			}
			Iterable<Observation> observations = observationRepository.filterByObservationIds(observationIdList);
			model.addAttribute("observations", observations);
		}
		return "observations";
	}
	
	@GetMapping (path="/export/{observationId}")
	public ResponseEntity<InputStreamResource> exportObservation(@PathVariable Integer observationId) throws IOException {
		Iterable<Attachment> attachments = attachmentRepository.getAttachmentsByObervationId(observationId);
		Observation observation = observationRepository.findOne(observationId);
		XWPFDocument doc= new XWPFDocument();
		XWPFParagraph paragraph = doc.createParagraph();
		
		addContent("Description", observation.getDescription(), paragraph);
		addContent("Build", observation.getBuild(), paragraph);
		addContent("Browser", observation.getBrowser(), paragraph);
		addContent("Steps", observation.getSteps(), paragraph);
		addContent("Actual Results", observation.getActualResults(), paragraph);
		addContent("Expected Results", observation.getExpectedResults(), paragraph);
		
		XWPFRun run = paragraph.createRun();  
		for(Attachment attachment: attachments) {
			try {
				XWPFPicture picture = run.addPicture(new ByteArrayInputStream(attachment.getFile()), XWPFDocument.PICTURE_TYPE_JPEG, attachment.getFileName(), Units.toEMU(400), Units.toEMU(400));
				run.addBreak();
				run.addBreak();
			} catch (InvalidFormatException | IOException e) {
				e.printStackTrace();
			}
		}
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
	    doc.write(bos);
	    bos.close();
		byte[] bytes = bos.toByteArray();
		HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename="+observation.getObservationId()+".docx");
        return ResponseEntity
                .ok()
                .headers(headers)
                .contentType(MediaType.APPLICATION_FORM_URLENCODED)
                .body(new InputStreamResource(new ByteArrayInputStream(bytes)));
	}

	private void addContent(String key, String value, XWPFParagraph paragraph) {
		XWPFRun paragraphOneRunOne = paragraph.createRun();
		paragraphOneRunOne.setBold(true);
		paragraphOneRunOne.setFontSize(12);
		paragraphOneRunOne.setFontFamily("Tahoma");
		paragraphOneRunOne.setText(key);
		paragraphOneRunOne.addBreak();
		if(null != value && value.contains("\n")) {
			XWPFRun paragraphOneRunTwo = paragraph.createRun();
			paragraphOneRunTwo.setFontSize(11);
			paragraphOneRunTwo.setFontFamily("Tahoma");
			String elements[] = value.split("\n");
			for(String element:elements) {
				paragraphOneRunTwo.setText(element);
				paragraphOneRunTwo.addBreak();
			}
		} else {
			XWPFRun paragraphOneRunTwo = paragraph.createRun();
			paragraphOneRunTwo.setFontSize(11);
			paragraphOneRunTwo.setFontFamily("Tahoma");
			paragraphOneRunTwo.setText(value);
			paragraphOneRunTwo.addBreak();
		}
		XWPFRun paragraphThree = paragraph.createRun();
		paragraphThree.addBreak();
	}
}