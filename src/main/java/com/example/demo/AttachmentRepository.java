package com.example.demo;

import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface AttachmentRepository extends CrudRepository<Attachment, Integer> {
	
	@Query("select u from Attachment u where u.observationId = ?1")
	Set<Attachment> getAttachmentsByObervationId(Integer observationId);
}
