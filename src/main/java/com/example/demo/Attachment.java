package com.example.demo;

import java.io.IOException;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.springframework.web.multipart.MultipartFile;

@Entity
public class Attachment {

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Integer attachmentId;
	
	private Integer observationId;
	
	private byte[] file;
	
	private String fileName;

	public String getFileName() {
		return fileName;
	}

	public Integer getAttachmentId() {
		return attachmentId;
	}

	public void setAttachmentId(Integer attachmentId) {
		this.attachmentId = attachmentId;
	}

	public Integer getObservationId() {
		return observationId;
	}

	public void setObservationId(Integer observationId) {
		this.observationId = observationId;
	}

	public byte[] getFile() {
		return file;
	}

	public void setFile(MultipartFile  file) {
		try {
			this.file = file.getBytes();
			fileName = file.getOriginalFilename();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
