package com.example.demo;

import java.util.List;
import java.util.Set;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface ObservationRepository extends CrudRepository<Observation, Integer> {

	@Query("select u from Observation u where u.createdBy = ?1 and u.reported = ?2")
	Set<Observation> filterByCreatedByReported(String createdBy, String reported);
	
	@Query("select u from Observation u where u.reported = ?1")
	Set<Observation> filterByReported(String reported);
	
	@Query("select u from Observation u where u.observationId in ?1")
	Set<Observation> filterByObservationIds(List<Integer> observationIds);
}
